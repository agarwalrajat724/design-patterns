package Creational.Singleton;

import java.nio.channels.AsynchronousFileChannel;

public class DbSingletonDemo {

    public static void main(String[] args) {
        DbSingleton dbSingleton1 = DbSingleton.getInstance();

        DbSingleton dbSingleton2 = DbSingleton.getInstance();

        System.out.println(dbSingleton1);

        System.out.println(dbSingleton2);

    }
}
