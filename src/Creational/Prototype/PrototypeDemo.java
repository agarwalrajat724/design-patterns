package Creational.Prototype;

public class PrototypeDemo {

    public static void main(String[] args) {

        Registry registry = new Registry();

        Movie movie = (Movie) registry.createRegistry(Movie.class.getSimpleName().toLowerCase());

        movie.setTitle("dgfhdghfd");

        System.out.println(movie);
        System.out.println(movie.getRuntime());
        System.out.println(movie.getTitle());

        Movie movie1 = (Movie) registry.createRegistry(Movie.class.getSimpleName().toLowerCase());

        movie.setTitle("sjdshgdhsd");


        System.out.println(movie1);
        System.out.println(movie1.getRuntime());
        System.out.println(movie1.getTitle());


    }
}
