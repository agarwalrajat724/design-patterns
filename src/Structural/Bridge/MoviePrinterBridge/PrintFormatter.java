package Structural.Bridge.MoviePrinterBridge;

import java.util.List;

public class PrintFormatter implements Formatter {

    @Override
    public String format(String header, List<Detail> detailList) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(header);
        stringBuilder.append("\n");

        for (Detail detail : detailList) {
            stringBuilder.append(detail.getLabel());
            stringBuilder.append(":");
            stringBuilder.append(detail.getValue());
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }
}
