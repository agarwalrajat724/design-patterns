package Structural.Bridge.ShapeDemo;

public class Green implements Color {

    @Override
    public void applyColor() {
        System.out.println("Applying Green Color...");
    }
}
