package Structural.Bridge.ShapeDemo;

public class Red implements Color {

    @Override
    public void applyColor() {
        System.out.println("Applying Red Color...");
    }
}
