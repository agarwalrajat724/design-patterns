package Creational.Factory;

import java.util.*;

public abstract class Website {

    protected List<Page> pages = new ArrayList<>();

    public List<Page> getPages() {
        return pages;
    }

    public Website() {

        this.createWebsites();
    }

    public abstract void createWebsites();
}
