package Structural.Bridge.ShapeDemo;

public interface Color {

    public void applyColor();
}
