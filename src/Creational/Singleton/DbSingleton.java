package Creational.Singleton;

public class DbSingleton {

    private static volatile DbSingleton instance = null;

    private DbSingleton() {
        if (null != instance) {
            System.out.println("Exception.....");
        }
    }

    public static DbSingleton getInstance() {

        if (null == instance) {
            synchronized (DbSingleton.class) {
                if (null == instance) {
                    instance = new DbSingleton();
                }
            }
        }

        return instance;
    }
}
