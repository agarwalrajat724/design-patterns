package Structural.Adapter;

public class EmployeeAdaptCSV implements Employee {

    private EmployeeCSV employeeAdaptCSV;

    public EmployeeAdaptCSV(EmployeeCSV employeeCSV) {

        this.employeeAdaptCSV = employeeCSV;
    }

    @Override
    public String getId() {
        return employeeAdaptCSV.getId() + "";
    }

    @Override
    public String getFirstName() {
        return employeeAdaptCSV.getFirstName();
    }

    @Override
    public String getLastName() {
        return employeeAdaptCSV.getLastName();
    }

    @Override
    public String getEmail() {
        return employeeAdaptCSV.getEmailAddress();
    }
}
