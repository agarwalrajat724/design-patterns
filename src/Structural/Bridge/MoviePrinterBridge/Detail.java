package Structural.Bridge.MoviePrinterBridge;

public class Detail {

    public String label;

    public String value;

    public Detail(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }
}
