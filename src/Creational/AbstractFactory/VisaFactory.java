package Creational.AbstractFactory;

public class VisaFactory extends CreditCardFactory {

    @Override
    public CreditCard getCreditCard(CreditCardType creditCardType) {

        switch (creditCardType) {
            case GOLD:{
                return new VisaGoldCreditCard();
            }

            case PLATINUM:{
                return new VisaPlatinumCreditCard();
            }
        }

        return null;
    }

    @Override
    public Validator getValidator(CreditCardType creditCardType) {

        switch (creditCardType) {

            case PLATINUM:{
                return new VisaPlatinumValidator();
            }

            case GOLD:{
                return new VisaGoldValidator();
            }
        }

        return null;
    }
}
