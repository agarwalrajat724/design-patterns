package Structural.Bridge.MoviePrinterBridge;

import java.util.List;

public abstract class Printer {


    public String print(Formatter formatter) {
        return formatter.format(getHeader(), getDetailList());
    }

    abstract protected List<Detail> getDetailList();

    abstract protected String getHeader();
}
