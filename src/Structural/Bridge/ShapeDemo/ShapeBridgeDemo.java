package Structural.Bridge.ShapeDemo;

public class ShapeBridgeDemo {

    public static void main(String[] args) {

        Color blue = new Blue();

        Color red = new Red();

        Color green = new Green();

        Shape shape = new Circle(blue);

        shape.applyColor();


        Shape square = new Square(red);

        square.applyColor();

        // We did not chage our Circle or Shape object at all to accomodate Green Color
        Shape greenCircle = new Circle(green);
        greenCircle.applyColor();


    }
}
