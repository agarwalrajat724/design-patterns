package Structural.Adapter;

import java.util.Arrays;
import java.util.List;

/**
 * Every day example of Array -> Lists
 *-> Array legacy,there are methods in Collection API to adapt
 * Array to Lists
 * -> Stream Classes in Input Output
 * Almost all the Stream Classes have Adapter to work with other
 * Streams or Readers.
 */
public class AdapterDesign {

    public static void main(String[] args) {
        Integer[] arrayOfInts = new Integer[] {42, 43, 44};

        List<Integer> integerList = Arrays.asList(arrayOfInts);

        System.out.println(arrayOfInts);

        System.out.println(integerList);
    }
}
