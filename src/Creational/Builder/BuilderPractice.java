package Creational.Builder;

public class BuilderPractice {


    private final String name;
    private final String age;
    private final String company;

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public String getCompany() {
        return company;
    }

    public BuilderPractice(Builder builder) {
        this.name = builder.name;
        this.age = builder.age;
        this.company = builder.company;
    }

    public static class Builder {
        private String name;
        private String age;
        private String company;

        public Builder(String name) {
            this.name(name);
        }

        private Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder age(String age) {
            this.age = age;
            return this;
        }

        public Builder company(String company) {
            this.company = company;
            return this;
        }

        public BuilderPractice build() {
            return new BuilderPractice(this);
        }
    }
}
