package Creational.Builder;

public class LunchOrderDemo {

    public static void main(String[] args) {

        // LunchOrder.Builder builder = new LunchOrder.Builder().bread("kjdksd").condiments("djfjd").dressing("sjdjs").meat("jsdjs");

        LunchOrder.Builder builder = new LunchOrder.Builder("sgdhsd","shgdhs")
                .condiments("sjgdhsd").meat("sjdjsd");

        LunchOrder lunchOrder = builder.build();

        System.out.println(lunchOrder.getBread());

        System.out.println(lunchOrder.getCondiments());

        System.out.println(lunchOrder.getDressing());

        System.out.println(lunchOrder.getMeat());


        BuilderPractice.Builder builder1 = new BuilderPractice.Builder("Hitti").age("1").company("Dhadeti");


        BuilderPractice builderPractice = builder1.build();

        System.out.println(builderPractice.getAge());

        System.out.println(builderPractice.getName());

        System.out.println(builderPractice.getCompany());

    }

}
