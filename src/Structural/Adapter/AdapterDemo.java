package Structural.Adapter;

public class AdapterDemo {

    public static void main(String[] args) {
        EmployeeClient employeeClient = new EmployeeClient();

        employeeClient.getEmployeeList().forEach(employee -> System.out.println(employee.todefaultString()));
    }
}
