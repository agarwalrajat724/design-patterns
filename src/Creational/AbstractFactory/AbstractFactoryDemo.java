package Creational.AbstractFactory;

public class AbstractFactoryDemo {


    public static void main(String[] args) {


        CreditCardFactory abstractFactory = CreditCardFactory.getCreditCardFactory(775);

        CreditCard creditCard = abstractFactory.getCreditCard(CreditCardType.PLATINUM);

        System.out.println(creditCard.getClass());

        abstractFactory = CreditCardFactory.getCreditCardFactory(600);

        creditCard = abstractFactory.getCreditCard(CreditCardType.GOLD);

        System.out.println(creditCard.getClass());
    }
}
