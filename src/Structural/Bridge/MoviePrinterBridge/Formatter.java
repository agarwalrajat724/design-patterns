package Structural.Bridge.MoviePrinterBridge;

import java.util.List;

public interface Formatter {

    String format(String header, List<Detail> detailList);
}
