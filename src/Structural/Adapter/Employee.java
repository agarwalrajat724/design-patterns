package Structural.Adapter;

public interface Employee {

    public String getId();

    public String getFirstName();

    public String getLastName();

    public String getEmail();

    default String todefaultString() {
        return "ID : " + getId() + " fName : " + getFirstName() + " lName :  " + getLastName() + " email : " + getEmail();
    }
}
