package Creational.AbstractFactory;

public class AmexFactory extends CreditCardFactory {

    @Override
    public CreditCard getCreditCard(CreditCardType creditCardType) {

        switch (creditCardType) {

            case GOLD: {
                return new AmexGoldCreditCard();
            }

            case PLATINUM:{
                return new AmexPlatinumCreditCard();
            }
        }

        return null;
    }

    @Override
    public Validator getValidator(CreditCardType creditCardType) {

        switch (creditCardType) {

            case PLATINUM:{
                return new AmexPlatinumValidator();
            }

            case GOLD:{
                return new AmexGoldValidator();
            }
        }

        return null;

    }


}
