package Creational.Prototype;

import java.util.HashMap;
import java.util.Map;

public class Registry {

    private Map<String, Item> items = new HashMap<>();

    public Registry() {

        loadItems();
    }

    public Item createRegistry(String key) {

        Item item = null;

        try {
            item =  (Item) items.get(key).clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return item;
    }

    private void loadItems() {

        Movie movie = new Movie();

        movie.setPrice(25);
        movie.setRuntime("2 hours");
        movie.setTitle("Default");

        items.put("movie", movie);

        Book book = new Book();

        book.setNumberOfPages(200);
        book.setPrice(29);
        book.setTitle("Default");

        items.put("book", book);

    }
}
