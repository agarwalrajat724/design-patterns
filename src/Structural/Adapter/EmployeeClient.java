package Structural.Adapter;

import java.util.ArrayList;
import java.util.List;

public class EmployeeClient {

    public List<Employee> getEmployeeList() {
        List<Employee> employees = new ArrayList<>();

        Employee employeeDB = new EmployeeDB("1", "Hitti", "Garg", "hittibabu@email.com");

        employees.add(employeeDB);


        // This Won't Work
        // Employee employeeLDAP = new EmployeeLDAP("adaptId", "adaptFirst", "adaptLast", "adapt@email.com");

        EmployeeLDAP employeeLDAP = new EmployeeLDAP("adaptId", "adaptFirst", "adaptLast",
                "adapt@email.com");

        employees.add(new EmployeeAdaptLDAP(employeeLDAP));

        EmployeeCSV employeeCSV = new EmployeeCSV("123,csvFirstName,csvLastName,csv@email.com");

        employees.add(new EmployeeAdaptCSV(employeeCSV));

        return employees;
    }
}
