package Structural.Adapter;

import java.util.List;
import java.util.StringTokenizer;

public class EmployeeCSV {

    private int id;

    private String firstName;

    private String lastName;

    private String emailAddress;

    public EmployeeCSV(String values) {

        StringTokenizer stringTokenizer = new StringTokenizer(values, ",");

        if (stringTokenizer.hasMoreElements()) {
            this.id = Integer.parseInt(stringTokenizer.nextToken());
        }

        if (stringTokenizer.hasMoreElements()) {
            this.firstName = stringTokenizer.nextToken();
        }

        if (stringTokenizer.hasMoreElements()) {
            this.lastName = stringTokenizer.nextToken();
        }

        if (stringTokenizer.hasMoreElements()) {
            this.emailAddress = stringTokenizer.nextToken();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
