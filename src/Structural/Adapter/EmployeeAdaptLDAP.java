package Structural.Adapter;

public class EmployeeAdaptLDAP implements Employee {

    private EmployeeLDAP instance;

    public EmployeeAdaptLDAP(EmployeeLDAP employeeLDAP) {

        this.instance = employeeLDAP;
    }

    @Override
    public String getId() {
        return instance.getCn();
    }

    @Override
    public String getFirstName() {
        return instance.givenName;
    }

    @Override
    public String getLastName() {
        return instance.surName;
    }

    @Override
    public String getEmail() {
        return instance.mail;
    }
}
