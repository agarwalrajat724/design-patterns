package Structural.Adapter;

public class EmployeeLDAP {

    public String cn;

    public String givenName;

    public String surName;

    public String mail;

    public EmployeeLDAP(String cn, String givenName, String surName, String lastName) {
        this.cn = cn;
        this.givenName = givenName;
        this.surName = surName;
        this.mail = lastName;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
