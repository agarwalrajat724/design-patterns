package Structural.Bridge;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Same as Adapter, but Adapter works with Legacy Code
 * while Bridge works with New Code
 */
public class BridgeCommonExample {


    public static void main(String[] args) throws SQLException {
        // DriverManager.registerDriver( /* Driver */ );

        String dbUrl = "database url";

        Connection connection = DriverManager.getConnection(dbUrl);

        Statement statement = connection.createStatement();

        // This client has a abstraction and can work with any database that has a driver
        statement.executeUpdate("CREATE TABLE ADDRESS (ID INT, street_name VARCHAR(20), city VARCHAR(20))");

        System.out.println("Table Created");
    }
}
