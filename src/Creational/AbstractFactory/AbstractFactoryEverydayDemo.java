package Creational.AbstractFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class AbstractFactoryEverydayDemo {

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {


        String xml = "<document><body><name>HITTI</name></body></document>";

        ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());

        DocumentBuilderFactory abstractFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder factory = abstractFactory.newDocumentBuilder();

        Document document = factory.parse(bais);

        document.getDocumentElement().normalize();

        System.out.println(" Root Element : " + document.getDocumentElement().getTagName());

        System.out.println(abstractFactory.getClass());

        System.out.println(factory.getClass());



    }
}
