package Structural.Bridge.MoviePrinterBridge;

public class BridgeDemo {

    public static void main(String[] args) {
        Movie movie = new Movie();

        movie.setClassification("Action");
        movie.setRuntime("2:15");
        movie.setTitle("John Wick");
        movie.setYear("2014");


        Formatter printFormatter = new PrintFormatter();
        Printer moviePrinter = new MoviePrinter(movie);

        String printedMaterial =  moviePrinter.print(printFormatter);


        System.out.println(printedMaterial);

    }
}
