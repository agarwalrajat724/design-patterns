package Creational;

public class DbSingleton {

    private volatile static DbSingleton instance = null;

    private DbSingleton() throws Exception {
        if (null != instance) {
            throw new Exception("fkjdhksd");
        }
    }

    public static DbSingleton getInstance() {
        if (null == instance) {
            synchronized (DbSingleton.class) {
                if (null == instance) {
                    try {
                        instance = new DbSingleton();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
Integer integer = new Integer(34);

        Integer integer1 = Integer.valueOf(34);
        return instance;
    }


    public static void main(String[] args) {
        Integer integer1 = Integer.valueOf(34);
        Integer integer = Integer.valueOf(34);

        System.out.println(integer.hashCode());
        System.out.println(integer1);


        if (integer == integer1) {
            System.out.println(true);
        }

        String s = "abc";

        String s1 = s.intern();


        System.out.println(s == s1);
    }
}
